Reverse IP Flask Application

This project is a Flask-based web application designed to retrieve and reverse the IP address of the client accessing the application. It incorporates a PostgreSQL database to store IP address records. The application is containerized using Docker and is deployable to a Kubernetes cluster. Deployment to Google Kubernetes Engine (GKE) is automated using GitLab CI/CD with a Helm chart.

Features
1. Reverse IP Lookup
The main feature of this application is its ability to retrieve the IP address of the client making a request to the application and reverse it. For example, if the client's IP address is 192.168.1.1, the application will return 1.1.168.192.

2. Flask Web Framework
The application is built using the Flask web framework, a lightweight and extensible Python web framework. Flask provides the necessary tools for handling HTTP requests, routing, and rendering HTML templates, making it well-suited for building web applications.

3. SQLAlchemy Database Interaction
To store IP address records, the application interacts with a PostgreSQL database using SQLAlchemy, a powerful SQL toolkit and Object-Relational Mapping (ORM) library for Python. SQLAlchemy simplifies database operations by allowing developers to work with database tables using Python classes and objects.

4. Docker Containerization
The application is containerized using Docker, a platform that enables developers to package applications and their dependencies into lightweight, portable containers. Docker containers ensure consistency in deployment environments and facilitate seamless deployment across different platforms.

5. Automated Deployment with GitLab CI/CD and Helm
Deployment of the application to a Kubernetes cluster is automated using GitLab CI/CD in conjunction with Helm, a package manager for Kubernetes. GitLab CI/CD pipelines fetch a Helm chart from a GitHub repository and deploy the application as a multicontainer pod in the GKE cluster. Helm simplifies the deployment process by providing a templating mechanism and versioned releases for Kubernetes applications.

6. Exposed Using Google Cloud Load Balancer
The application is exposed to the internet using a Google Cloud Load Balancer. This load balancer distributes incoming traffic across multiple instances of the application running in the GKE cluster, providing scalability, high availability, and fault tolerance.


Setup
The setup process involves configuring the environment, installing dependencies, and deploying the application to GKE cluster.

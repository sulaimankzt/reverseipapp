import unittest
from app import app

class TestRoutes(unittest.TestCase):

    def setUp(self):
        # Create a test client using Flask's test client
        self.app = app.test_client()
        self.app.testing = True

    def test_home_route(self):
        # Send a GET request to the home route
        response = self.app.get("/")
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import os

# Database connection details from GitLab variables
db_host = os.environ.get("db_host")
db_port = os.environ.get("db_port")
db_user = os.environ.get("db_user")
db_password = os.environ.get("db_password")
db_name = os.environ.get("db_name")

app = Flask(__name__)

# Configure database connection using environment variables
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

# Database models
class ReverseIP(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    original_ip = db.Column(db.String(15), nullable=False)
    reversed_ip = db.Column(db.String(15), nullable=False)

def create_tables():
    with app.app_context():
        db.create_all()

# Creating tables if they don't exist
create_tables()

@app.route("/")
def reverse_ip():
    client_ip = request.remote_addr
    reversed_ip = ".".join(reversed(client_ip.split(".")))
    
    # Store the IP in the database
    new_entry = ReverseIP(original_ip=client_ip, reversed_ip=reversed_ip)
    db.session.add(new_entry)
    db.session.commit()

    return f"Your reversed IP is: {reversed_ip}"  # Corrected response

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
